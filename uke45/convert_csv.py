# Ikke pensum!
import pandas as pd 
import matplotlib.pyplot as plt 

flix = pd.read_csv("netflix_titles.csv")

# Bare hiver ut en del ting, som director
flix.drop(["cast", "director"], axis=1, inplace=True)

# ...og der dato lagt til samt rating er null
flix = flix[flix["date_added"].isnull() == False]
flix = flix[flix["rating"].isnull()==False]

'''
Skrive til fil. Hele poenget her er at siden det finnes
elementer som inneholder komma, da kan ikke csv-filen
reparere på denne. Pandas som brukes ovenfor er flinkere.
Derfor må jeg importere filen med rådata, og lagre på
nytt med en annen separator (som jeg håper at ikke
roter til like mye. Men vi får se!)
'''
flix.to_csv("netflix_sane.csv", sep='#')