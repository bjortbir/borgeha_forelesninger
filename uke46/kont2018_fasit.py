notebook = {}

def addSite(notebook):
    site = input("Nettsted: ")
    if site in notebook.keys():
        print("Nettstedet har allerede en bruker")
        return notebook
    else:
        username = input("Username: ")
        pwd = input("pssword: ")
        notebook[site] = [username, [pwd]]
        return notebook

notebook = {'facebook': ['urgle', ['pwd','newpass']], \
            'vgeretfrykteligkortnavn': ['user', ['pwd2']], \
            'db': ['nah', ['pwd']]}


def showSites(notebook):
    print(f'{"Nettsted:":17}{"Brukernavn:":15}{"Passord:"}')
    for site, val in notebook.items():
        print(f'{site[:15]:17}{val[0]:15}{val[1][-1]}')

#showSites(notebook)

def formatList(list):
    return ", ".join(list)

#print(formatList([str(x) for x in range(10)]))

def editSite(notebook, site):
    while True:
        newpwd = input(f"Add a new password for {site}: ")
        if newpwd in notebook[site][1]:
            print(f"'{newpwd}' has been used for {site} already.")
            print(f'The following password have been used: {formatList(notebook[site][1])}.')
        else:
            notebook[site][1].append(newpwd)
            return notebook

#editSite(notebook, 'facebook')

def secureSites(notebook):
    passFrequency = {}
    for site, val in notebook.items():
        lastpass = val[1][-1]
        usedinsites = passFrequency.get(lastpass, [])
        usedinsites.append(site)
        passFrequency[lastpass] = usedinsites
    allUnique = True
    for pwd, sites in passFrequency.items():
        if len(sites) > 1:
            print(f'You have used "{pwd}" at these sites: {formatList(sites)}.')
            allUnique = False
    if allUnique:
        print("No passwords used at many sites. Great work!")

#secureSites(notebook)    
    
    
def importResults(file):
    
    while file != 'q':
        try:
            with open(file,'r', encoding='utf-8') as f:
                linjer = f.readlines()
                for i in range(len(linjer)):
                    linjer[i] = linjer[i].strip()
                return linjer
        except:
            print(f"The file {file} could not be found.")
            file = input("File name 'q' exits: ")

results = importResults('Matches.txt')

def analyseResults(results):
    tmp = []
    for linje in results:
        h, b, r = linje.split(',')
        hm = int(r.split('-')[0])
        bm = int(r.split('-')[1])
        tmp.append([h, b, hm, bm])
    return tmp

analyzed = analyseResults(results)

def calculateScores(h, b):
    if h > b:
        return 3, 0
    if h == b:
        return 1, 1
    return 0, 3

# print(calculateScores(11, 11))

def sumTeamValues(analyzed):
    dict = {}
    for ht, at, hg, ag in analyzed:
        hsc, asc = calculateScores(hg, ag)
        hvalues = dict.get(ht, [0, 0])
        avalues = dict.get(at, [0, 0])
        hvalues[0] += hsc
        hvalues[1] += 1
        avalues[0] += asc
        avalues[1] += 1
        dict[ht] = hvalues
        dict[at] = avalues
    return dict

team_data = sumTeamValues(analyzed)
                
# Merk her: Den skal skrive ut resultatene fra oppgave B
def showResults(analyzed): 
    print('#'*45)
    for ht, at, hg, ag in analyzed:
            if hg > ag:
                l = 'H'
            elif hg < ag:
                l = 'B'
            else:
                l = 'U'
            print(f'# {ht:15}{at:15}{hg:2} - {ag:2} ({l}) #')
    print('#'*45)
    
showResults(analyseResults(results))      
print('')    
        
def savePoints(team_data):
    with open('Points.txt', 'w') as f:
        # Erstatte de under med f.write()
        print('#'*35)
        print(f'# {"Navn":16}{"Poeng":7} Kamper  #')
        
        # Sortering:
        team_list = []
        for t in team_data.items():
            # team_list now looks like this: [[points, teamname, matches],[p,t,m]...]
            team_list.append([t[1][0], t[0], t[1][1]])
        
        team_list = sorted(team_list, reverse=True)
        for team in team_list:
            print(f'# {team[1]:15} {team[0]:<8}{team[2]:<6} #')
        print('#'*35)
            
savePoints(team_data)