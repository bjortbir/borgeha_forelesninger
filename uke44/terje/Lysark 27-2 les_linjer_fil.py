filnavn = input('Oppgi navn på fila: ')
teller = 1                   # Skal telle antall linjer
try:
    f = open(filnavn,'r')    # Åpner fila for lesing
    linje = f.readline()     # Leser første linje
    while linje:
        linje = linje.strip()  # Fjern linjeskift
        print(f'{teller}\t{linje}')
        linje = f.readline()
        teller += 1
    f.close()
except IOError:
    print('Finner ikke fila.')